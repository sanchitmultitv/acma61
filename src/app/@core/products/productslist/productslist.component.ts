import { AfterViewInit, Component, HostListener, OnInit } from '@angular/core';
declare var $:any;
@Component({
  selector: 'app-productslist',
  templateUrl: './productslist.component.html',
  styleUrls: ['./productslist.component.scss']
})
export class ProductslistComponent implements OnInit, AfterViewInit {
  showIndex;
  message ='nothing';
  showing = false;
  listImgs = [
    {img:'assets/ipf/images/products/img1.jpeg'},
    {img:'assets/ipf/images/products/img2.jpeg'},
    {img:'assets/ipf/images/products/img3.jpeg'},
    {img:'assets/ipf/images/products/img4.jpeg'},
    {img:'assets/ipf/images/products/img5.jpeg'},
    {img:'assets/ipf/images/products/img6.jpeg'},
    {img:'assets/ipf/images/products/img7.jpeg'},
  ];
  scrollerCount=0;
  listedImg;
  scrollerHeight = 0;
  constructor() { }

  ngOnInit(): void {
    let up:any = document.getElementById('up');
    let down:any = document.getElementById('down');
    up.style.display='none';
    this.onShowImage(this.listImgs[0]['img']);
  }
  
  
  onShowImage(img){
    this.listedImg=img;
    this.showIndex = this.listImgs.findIndex((ele:any) => ele.img === img);
    // set zoom bg img
    let zoom:any = document.getElementById('zoom');
    zoom.style.backgroundImage = "url('" + img + "')";
  }
  onScrollup(){
    let slideImages:any = document.getElementById('slideImages');
    let listSliderImg:any = document.getElementById('listSliderImg');
    this.scrollerCount--;    
    this.scrollerHeight = 104 * this.scrollerCount;   
    slideImages.scrollTop = this.scrollerHeight;    
    listSliderImg.style.tranistion = '.3s';
    if(this.scrollerHeight < 104){
      document.getElementById('down').style.display = 'block';
      document.getElementById('up').style.display = 'none';
    }
    if((this.scrollerHeight < 208)&&(this.scrollerHeight >= 104)){
      document.getElementById('down').style.display = 'block';
    }
  }
  onScrolldown(){
    let slideImages:any = document.getElementById('slideImages');
    let listSliderImg:any = document.getElementById('listSliderImg');
    this.scrollerCount++; 
    this.scrollerHeight = 104 * this.scrollerCount;   
    slideImages.scrollTop = this.scrollerHeight;
    listSliderImg.style.tranistion = '.3s';
    if(this.scrollerHeight == 208){
      document.getElementById('down').style.display = 'none';
      document.getElementById('up').style.display = 'block';
    }
    if((this.scrollerHeight < 208)&&(this.scrollerHeight >= 104)){
      document.getElementById('up').style.display = 'block';
    }
    console.log('scrollheight', )
  }

  @HostListener('document:mousemove', ['$event', '$event.target'])
  onClick(event: MouseEvent, targetElement: HTMLElement): void {
    let htmlEvent = document.getElementById('boximgid'); 
    if(targetElement == htmlEvent){
      this.move(event);
    }
  }
  hideFull(event){
    this.showing = false;
  }
  ngAfterViewInit(){
    this.loadingZoomImage();
  }
  boxwidth;
  boxHeight;
  selectorwidth;
  selectorHeight;
  loadingZoomImage(){
    let maincontainer:any = document.querySelector('.boximgid');
    this.boxwidth = maincontainer.offsetWidth;
    this.boxHeight = maincontainer.offsetHeight;
    let rect:any = document.querySelector('.rect');
    let w2 = rect.offsetWidth;
    let h2 = rect.offsetHeight;
    this.selectorwidth = w2/2;
    this.selectorHeight = h2/2;
    // initiate zoom
    let zoom:any = document.querySelector('.zoom');
    // set zoom bg img
    zoom.style.backgroundImage = "url('" + maincontainer.src + "')";
    let ratio = 5;
    zoom.style.backgroundSize = this.boxwidth * ratio + 'px ' + this.boxHeight * ratio + 'px';
    zoom.style.width = w2 * ratio + 'px';
    zoom.style.height = h2 * ratio + 'px';
  }
  move(event){
    let ratio = 5;
    let rect:any = document.querySelector('.rect');
    // rect.style.transform = 'translate(-50%, -50%)';
    let x = event.offsetX;
    let y = event.offsetY; 
    let zoomX = x * ratio;
    let zoomY = y * ratio;
    // if(x < this.selectorwidth){
    //   x = this.selectorwidth;
    //   // matching zoom with selector
    //   zoomX = 0;
    // }
    // if(x > (this.boxwidth - this.selectorwidth)){
    //   x = this.boxwidth - this.selectorwidth;
    //   // zoomX = x - this.selectorwidth;
    // }
    // if(y < this.selectorHeight){
    //   y = this.selectorHeight;
    //   // zoomY = 0;
    // }
    // if(y > (this.boxHeight - this.selectorHeight)){
    //   y = this.boxHeight - this.selectorHeight;
    // }
    if (x > this.boxwidth - rect.offsetWidth) { x = this.boxwidth- rect.offsetWidth; }
    if (x < 0) { x = 0; }
    if (y > this.boxHeight - rect.offsetHeight) { y = this.boxHeight - rect.offsetHeight; }
    if (y < 0) { y = 0; }
    rect.style.top = y + 'px';
    rect.style.left = x + 'px';
    // initiate zoom position
    let zoom:any = document.querySelector('.zoom');
    zoom.style.display='block';
    zoom.style.backgroundPosition = '-' + x*5 + 'px ' + '-' + y*5 + 'px';
  }
  mouseOut(){
    let zoom:any = document.querySelector('.zoom');
    zoom.style.display='none';
  }
}

