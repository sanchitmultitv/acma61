import { Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  constructor(private socket: Socket) { }
  
  public getconnect(token) {
    this.socket.emit('bigboy', {
      token: token,
    });
    console.log(token);
  }
  public getSocketMessages = (token) => {
    return Observable.create((observer) => {
      this.socket.on(token, (message) => {
        console.log('msg', message);
        observer.next(message);
      });
    });
  }

  public getMessages = () => {
    return Observable.create((observer) => {
      this.socket.on('toujeo-172', (message) => {

        observer.next(message);
      });
    });
  }

  public sendMessage(message, usr, video_id) {
    //console.log('servicce',message);
    this.socket.emit('sendchat', message);
    this.socket.emit('notify_user', usr, video_id);
    // console.log(usr, message, video_id);
  }
  public sendMessageAudiOne(message, usr, video_id, event_id) {
    //console.log('servicce',message);
    this.socket.emit('sendchat', message);
    this.socket.emit('notify_user', usr, video_id, event_id);
    console.log(usr, message, video_id);
  }
  public addUser(usr, video_id) {
    this.socket.emit('adduser', {
      username: usr,
     // avatarId: avatar_id,
      video_id: video_id,
      video_name: 'demo test',
    });
  }


  public receiveMessages = (room) => {
    return Observable.create((observer) => {
      this.socket.on('updatechat', (user_name, roomId, chat_data) => {
        let meessageObject = { chat_data, user_name, room, roomId };
        if (user_name === 'SERVER') {
          console.log(roomId + '=====' + user_name );
          // observer.next(chat_data);
        } else if (localStorage.getItem('name') == user_name) {
          console.log(roomId + '=====' + user_name );
          observer.next(meessageObject);
        } else {
          console.log(roomId + '=====' + user_name );
          observer.next(meessageObject);
        }
        //console.log('sevrc',message);
      });
    });
  };
  getgroupMessage() {
    return Observable.create((obs) => {
      this.socket.on('toujeo-162', (msg) => {
        obs.next(msg);
        console.log('msg', msg);
      });
    });
  }

  public getGroupChatMessages = () => {
    return Observable.create((observer) => {
      this.socket.on('updatechat', (user_name, avatarId, chat_data) => {
       let meessageObject = {chat_data,user_name};
       if (user_name === 'SERVER') {
          // console.log(
          //   user_name + '========' + avatarId + '==============' + chat_data
          // );
         // observer.next(chat_data);
        } else if (localStorage.getItem('username') == user_name) {
          // console.log(
          //   user_name + '========' + avatarId + '==============' + chat_data
          // );
          observer.next(meessageObject);
        } else {
          // console.log(
          //   user_name + '========' + avatarId + '==============' + chat_data
          // );
          observer.next(meessageObject);
        }
        //console.log('sevrc',message);
      });
    });
  };
  disconnect(){
    this.socket.on('disconnect', ()=>{
      console.log('disconnn')

    }); 
  }
}
