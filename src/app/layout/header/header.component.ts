import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FetchDataService } from 'src/app/services/fetch-data.service';
import { SteupServiceService } from 'src/app/services/steup-service.service';
import { MenuItems } from '../shared/menu.items';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  menuItems = MenuItems;
  constructor(private router: Router, private _analytics: SteupServiceService) { }

  ngOnInit(): void {
  }
  exit(){
    localStorage.removeItem('virtual');
    this.router.navigate(['/login']);
  }
  stepup(action){
    this._analytics.stepUpAnalytics(action);
  }
}
