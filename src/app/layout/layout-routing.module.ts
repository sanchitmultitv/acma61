import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BreakoutComponent } from '../@core/breakout/breakout.component';
import { CometComponent } from '../@core/comet/comet.component';
import { GameComponent } from '../@core/game/game.component';
import { HexagonComponent } from '../@core/hexagon/hexagon.component';
import { SupportComponent } from './components/support/support.component';
import { LayoutComponent } from './layout.component';


const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', redirectTo: 'lobby', pathMatch: 'prefix' },
      { path: 'lobby', loadChildren: () => import('../@core/lobby/lobby.module').then(m => m.LobbyModule) },
      { path: 'lounge', loadChildren: () => import('../@core/networking-lounge/networking-lounge.module').then(m => m.NetworkingLoungeModule) },
      { path: 'photobooth', loadChildren: () => import('../@core/photobooth/photobooth.module').then(m => m.PhotoboothModule) },
      { path: 'eventhall', loadChildren: () => import('../@core/event-hall/event-hall.module').then(m => m.EventHallModule) },
      { path: 'gratitudewall', loadChildren: () => import('../@core/gratitude-wall/gratitude-wall.module').then(m => m.GratitudeWallModule) },
      { path: 'meetings', component: GameComponent },
      { path: 'breakout-one', component: BreakoutComponent },
      { path: 'network', component: CometComponent },
      { path: 'hexagon', component: HexagonComponent },
      { path: 'support', component: SupportComponent },
      { path: 'eventhall2', loadChildren: () => import('../@core/eventhall2/eventhall2.module').then(m => m.Eventhall2Module) },
      { path: 'eventhall3', loadChildren: () => import('../@core/eventhall3/eventhall3.module').then(m => m.Eventhall3Module) },
      // {path: 'speakers', loadChildren: ()=> import('../@core/speaker-profile/speaker-profile.module').then(m => m.SpeakerProfileModule)},
      // {path: 'products', loadChildren: ()=> import('../@core/products/products.module').then(m => m.ProductsModule)},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }
