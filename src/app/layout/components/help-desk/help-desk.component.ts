import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';

@Component({
  selector: 'app-help-desk',
  templateUrl: './help-desk.component.html',
  styleUrls: ['./help-desk.component.scss']
})
export class HelpDeskComponent implements OnInit {
  token = 'toujeo-172'; 
  deskMessages = [];
  message = new FormControl('');
  user_name;
  constructor(private chat: ChatService, private _fd: FetchDataService) { }

  ngOnInit(): void {
    this.gethelpdeskQuestions();
    this.chat.getconnect(this.token);
    this.chat.getSocketMessages(this.token).subscribe((res:any)=>{
      if(res.includes('question_reply')){
        this.gethelpdeskQuestions();
      }
      console.log('testing', res);
    });
  }
  gethelpdeskQuestions(){
      let data = JSON.parse(localStorage.getItem('virtual'));
      this.user_name = data.name;
      let event_id = '172';
      this._fd.gethelpdeskanswers(data.id, event_id).subscribe((res=>{
        this.deskMessages = res.result;
      }))
  
    }
  
  postHelpdesk(value){
    let data = JSON.parse(localStorage.getItem('virtual'));
    let event_id = '172';
    if((value !== '')&&(value !== null)){
      this._fd.helpdesk(data.id,value, event_id).subscribe((res=>{
        if(res.code == 1){
          this.message.reset();
          let arr ={
            "question": value,
            "answer": ""
          };
          this.deskMessages.push(arr);
        }
      }));
    }
  }
}
