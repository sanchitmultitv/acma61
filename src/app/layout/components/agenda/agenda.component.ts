// import { Component, OnInit } from '@angular/core';
import { AfterViewInit, Component, HostListener, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { ChatService } from 'src/app/services/chat.service';
import { FetchDataService } from 'src/app/services/fetch-data.service';
declare var $:any;
@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.scss']
})
export class AgendaComponent implements OnInit {
  agendas=[
    'assets/ipf/images/agenda/day-1.png',
    'assets/ipf/images/agenda/day-2.png',
    'assets/ipf/images/agenda/day-3.png'
  ];
  agenda;
  textMessage = new FormControl('');
  alertMsg
  agendaDay;
  day;
  points=[];
  constructor(private router: Router, private chatService: ChatService, private _fd: FetchDataService) { }

  ngOnInit(): void {
    let date = new Date().getDate();
    // let left:any = document.getElementById('previous');
    // left.style.display='none';
    if(date <= 24){
      this.agenda = this.agendas[0];
      this.agendaDay = 1;
    }
    if(date == 25 ){
      this.agenda = this.agendas[1];
      this.agendaDay = 2;
    }
    if(date >= 26){
      this.agenda = this.agendas[2];
      this.agendaDay = 3;
    }
  }
  closeAgenda(){
    $('#agendaModal').modal('hide');
  }
  postQuestion(value){
    let data = JSON.parse(localStorage.getItem('virtual'));
    let audi_id = '230'
    this._fd.askLiveQuestions(data.id,value,audi_id).subscribe((res=>{
      if(res.code == 1){
         this.alertMsg = 'Submitted Succesfully';
        setTimeout(() => {
          $('#agendaModal').modal('hide');
          this.router.navigate(['/lobby']); 
        }, 2000);
      }
      this.textMessage.reset(); 
    }));
  }
  closeChat(){
    this.router.navigate(['/lobby']);
  }
  showprevious(){
    let left:any = document.getElementById('previous');
    let right:any = document.getElementById('next');
    if(this.agendaDay === 2){
      this.agendaDay = 1;
      left.style.display = 'none';
      this.points = [];
      for (let i = 0; i < 13; i++) {
        this.points.push(i+1);
      }
    }
    if(this.agendaDay === 3){
      this.agendaDay = 2
      right.style.display = 'block';
      this.points = [];
      for (let i = 12; i < 25; i++) {
        this.points.push(i+1);
      }
    }
  }
  shownext(){
    let right:any = document.getElementById('next');
    let left:any = document.getElementById('previous');
    left.style.display='block';
    if(this.agendaDay === 2){
      this.agendaDay = 3;
      right.style.display = 'none';
    }
    if(this.agendaDay === 1){
      this.agendaDay = 2
      left.style.display = 'block';
    }
  }
}
