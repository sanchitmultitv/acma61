export const MenuItems = [
    {
        path: '/lobby',
        icon: 'assets/l.png',
        name: 'lobby',
        analytics: 'click_lobby'
    },
    // {
    //     datatarget: 'gratitudewall',
    //     icon: 'assets/ipf/icons/new/2.png',
    //     name: 'agenda',
    //     analytics: 'click_agenda'
    // },
    // {
    //     datatarget: 'speakersModal',
    //     icon: 'assets/ipf/icons/new/3.png',
    //     name: 'speaker profile',
    //     analytics: 'click_speakerprofile'
    // },
    {
        datatarget: 'feedbackModal',
        icon: 'assets/a.png',
        name: 'Agenda',
        analytics: 'click_agenda'
    },
    // {
    //     path: '/eventhall',
    //     icon: 'assets/ipf/icons/new/4.png',
    //     name: 'conference',
    //     analytics: 'auditorium_230'
    // },
    {
        path: '/photobooth',
        icon: 'assets/p.png',
        name: 'photo-op',
        analytics: 'click_photobooth'
    },
    // {
    //     path: '/hexagon',
    //     icon: 'assets/ipf/icons/new/5.png',
    //     name: 'lounge',
    //     analytics: 'click_lounge'
    // },
    {
        datatarget: 'agendaModal',
        icon: 'assets/q.png',
        name: 'q&a',
        analytics: 'click_breakout'
    },
    // {
    //     path: '/breakout-two',
    //     icon: 'assets/ipf/icons/new/7.png',
    //     name: 'Breakout 2',
    //     analytics: 'click_engangementzone'
    // },
    // {
    //     path: '/support',
    //     icon: 'assets/ipf/icons/new/8.png',
    //     name: 'help desk',
    //     analytics: 'click_helpdesk'
    // },
    
    // {
    //     path: '/login',
    //     icon: 'assets/ipf/icons/new/10.png',
    //     name: 'exit',
    //     analytics: 'click_logout'
    // },
];